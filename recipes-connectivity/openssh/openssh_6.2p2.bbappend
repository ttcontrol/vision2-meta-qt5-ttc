# By default, openssh conflicts with dropbear, as both provide
# ssh-daemon. As the QtCreator is setup to download files via SFTP,
# and dropbear does not provide SFTP support, we only use
# the package openssh-sftp-server in our image and keep dropbear
# for SSH/scp. Thus, no conflicts openssh <-> dropbear will arise. 
RCONFLICTS_${PN} = ""
RCONFLICTS_${PN}-sshd = ""
